// Please only write valid ES5 here
// uglify gets angry if you don't
// -- Tony June 9th, 2017

var rules = {
  football: {
    goal:        { forward: 4, midfielder: 5, defender: 6, goalkeeper: 6 },
    clean_sheet: {             midfielder: 1, defender: 4, goalkeeper: 4 },
    conceded_goal: {                          defender:-1, goalkeeper:-1, n: 2 },

    assist:          {all: 3},

    own_goal:     {all:-2},
    penalty_miss: {all:-2},

    keeper_save:  {all: 1, n: 3},
    penalty_save: {all: 5},

    yellow_card: {all:-1},
    red_card:    {all:-3},

    playtime1:  {all: 1},
    playtime60: {all: 1},
    playtime90: {all: 1},

    shootout_caused: {all: -2},
    penalty_caused: {all: -2},
    impact:          {all:  1},
    impact_positive: {all:  1},
    impact_negative: {all: -1}
  },
  hockey: {
    goal:               {forward: 5, defender:  8, goalkeeper: 10},
    power_play_goal:    {forward: 4, defender:  7, goalkeeper:  9},
    box_play_goal:      {forward: 7, defender: 10, goalkeeper: 12},

    assist:             {forward: 3, defender:  4, goalkeeper:  5},
    power_play_assist:  {forward: 2, defender:  3, goalkeeper:  4},
    box_play_assist:    {forward: 5, defender:  6, goalkeeper:  7},

    power_play: {all: -1},
    box_play: {all: 2},

    shot: {all: 0.5},
    plus_minus: {forward: 1, defender: 1},
    plus: {all: 1},
    minus: {all: -1},
    conceded_goal: {goalkeeper: -1},
    shootout_caused: {all: -3},

    team_victory: {goalkeeper: 3},
    overtime_victory: {goalkeeper: 2},
    keeper_save: {all: 0.2},
    clean_sheet: {goalkeeper: 4},

    playtime1:  {defender: 1, forward: 1},
    playtime15: {defender: 1, forward: 1},

    penalty2: {all: -1},
    penalty4: {all: -2},
    penalty5: {all: -2},
    penalty10: {all: -1},
    penalty20: {all: -2},
    shootout_caused: {all: -3},

    hit: {all: 0.5},
  },
  basket: {
    threepoint_goal: 3.5,
    field_goal: 2,
    freethrow_goal: 1,
    rebound: 1.25,
    assist: 1.5,
    steal: 2,
    block: 2,
    turnover: -0.5,
    double_double: 1.5,
    triple_double: 3
  },
  golf: {
    // Daily
    under70: 4,
    under65: 10,
    birdie: 3,
    birdie_birdie: 2,
    eagle: 7,
    double_eagle: 15,
    hole_in_one: 25,
    par: 1,
    bogey: -1,
    double_bogey: -4,
    water: -7,
    out_of_bounds: -10,
    three_put: -3,
    missed_gir: -1,
    missed_put: -2,
    put15: 3,
    put25: 6,
    sandy: 2,
    bunker: -1,
    fairway: 1,

    // Weekly
    made_cut: 10,
    missed_cut: -5,
    rank1: 15,
    rank2: 10,
    rank3: 5,
    top10: 2,
    up10: 3,
    up20: 6,
    major_league: 10,
    world_championship: 5,
  },
  dota2: {
    kills:           {core:  2, support:  3},
    deaths:          {core: -3, support: -2},
    assists:         {core:  2, support:  3},
    denies:         {all:2, n: 10},
    last_hits:      {core: 3, support: 4, n:100},
    gold_per_min:   {core: 2, support: 3, n:100},
    xp_per_min:     {core: 2, support: 3, n:100},
    hero_damage:     {core: 2, support: 4, n:1000},
    tower_damage:     {core: 1, support: 3, n:500},
    hero_healing:     {core: 1, support: 4, n:500},
  },
  horses: {
    rank1: {elite: 3000, v75: 3000, age3: 3000, age4: 3000, coldblood: 3000, rider: 120, coach: 120},
    rank2: {elite: 1500, v75: 1500, age3: 1500, age4: 1500, coldblood: 1500, rider: 60, coach: 60},
    rank3: {elite: 500, v75: 500,age3: 500, age4: 500,coldblood: 500, rider: 20, coach: 20},
    finished: {elite: 100, v75: 100,age3: 100, age4: 100,coldblood: 100, rider: 4, coach: 4},
    finished_major: {elite: 100, v75: 100, age3: 100, age4: 100, coldblood: 100, rider: 4, coach: 4},
    rank1_major: {elite: 3000, v75: 3000, age3: 3000, age4: 3000, coldblood: 3000, rider: 120, coach: 120},
    rank2_major: {elite: 1500, v75: 1500, age3: 1500, age4: 1500, coldblood: 1500, rider: 60, coach: 60},
    rank3_major: {elite: 500, v75: 500, age3: 500, age4: 500, coldblood: 500, rider: 20, coach: 20},
    prize_money: {elite: 1, v75: 1, age3: 1, age4: 1, coldblood: 1, rider: 0.04, coach: 0.04, n: 100},
    disqualified: {rider: -100, coach: -100},
    gallop: {rider: -20, coach: -20},
  },
  cycling: {
    top49: 3,
    top99: 2,
    finished: 1,
    team_victory: 10,
    yellow_shirt: 20,
    green_shirt: 20,
    climber_shirt: 20,
    youth_shirt: 20,
    aggressive: 25,
  },
  cycling_vm: {
    top49: 3,
    top99: 2,
    finished: 1,
    team_victory: 10,
  },
  cc_ski: {
    top29: 3,
    top44: 2,
    finished: 1,
    winner_shirt: 20,
    heat0_top10: 4,
    heat0_top20: 3,
    heat0_top30: 2,
    heat_pass: 5,
  }
}

function addLadder(obj, ladder, keyCb) {
  for(var i = 0; i < ladder.length; ++i) {
    obj[keyCb(i)] = {all: ladder[i]}
  }
}
function addLadders(obj, ladders) {
  Object.keys(ladders).forEach(function(key) {
    addLadder(obj, ladders[key], function(i) {
      return key + (i + 1)
    })
  })
}

exports.ladders = {
  cycling: {
    rank: [100, 80, 60, 50, 40, 36, 32, 28, 24, 21, 18, 15, 12, 10, 8, 7, 6, 5, 4],
    sprint_rank: [20, 17, 15, 13, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
    climb0_rank: [60, 48, 36, 24, 12, 6],
    climb1_rank: [30, 24, 18, 12, 6, 3],
    climb2_rank: [15, 9, 6, 3],
    climb3_rank: [6, 3],
    climb4_rank: [3],
  },
  cycling_vm: {
    rank: [100, 80, 60, 50, 40, 36, 32, 28, 24, 21, 18, 15, 12, 10, 8, 7, 6, 5, 4],
  },
  cc_ski: {
    rank: [100, 80, 60, 50, 40, 36, 32, 28, 24, 21, 18, 15, 12, 10, 8, 7, 6, 5, 4],
    sprint_rank: [20, 17, 15, 13, 11, 10, 9, 8, 7, 6],
    time0_rank: [25, 20, 15, 12, 10, 9, 8, 7, 6, 3],
    time1_rank: [50, 40, 30, 24, 20, 18, 16, 14, 12, 6],
    heat0_rank: [15, 12, 10, 8, 7, 6, 5],
    heat1_rank: [21, 15, 10, 6, 3, 1],
    heat2_rank: [30, 20, 12, 7, 4, 2],
    heat3_rank: [60, 40, 25, 15, 10, 5],
  }
}

Object.keys(exports.ladders).forEach(function(category) {
  addLadders(rules[category], exports.ladders[category])
})

const rankRegex = /(^|_)(top|rank)\d|^finished$/

Object.keys(rules.cycling).forEach(key => {
  var value = rules.cycling[key]
  if(isNaN(value)) value = value.all
  rules.cycling[key] = {
    captain: 2 * value,
    sprinter: 2 * value,
    climber: 3 * value,
    tempo: 3 * value,
    youth: 4 * value,
    support: 4 * value,
  }
})

Object.keys(rules.cc_ski).forEach(key => {
  var value = rules.cc_ski[key]
  if(isNaN(value)) value = value.all
  rules.cc_ski[key] = {
    men: value,
    women: value,
  }
})

rules.cycling.rank1.manager = 160
rules.cycling.rank2.manager = 100
rules.cycling.rank3.manager = 60
rules.cycling.rank4.manager = 40
rules.cycling.rank5.manager = 20

rules.cc_ski.rank1.manager = 24
rules.cc_ski.rank2.manager = 18
rules.cc_ski.rank3.manager = 12
rules.cc_ski.rank4.manager = 7
rules.cc_ski.rank5.manager = 4

exports.rules = rules

exports.playtimes = {
  football: {all: [1, 60, 90]},
  hockey: {defender: [1, 15], forward: [1, 15]}
}

exports.positions = {
  goalkeeper: 0,
  defender: 1,
  midfielder: 2,
  forward: 3,

  guard_point: 21,
  guard_shooting: 22,
  forward_small: 23,
  forward_power: 24,
  center: 25,
  core: 26,
  support: 27,

  elite: 60,
  v75: 61,
  age3: 63,
  age4: 64,
  coldblood: 67,
  rider: 68,
  coach: 69,

  manager: 70,
  captain: 71,
  tempo: 72,
  youth: 73,
  climber: 74,
  sprinter: 75,

  outsider: 81,
  chaff: 82,

  men: 90,
  women: 91,
}

exports.sportPositions = {
  football: [
    'goalkeeper',
    'defender',
    'midfielder',
    'forward',
  ],
  flexi_five: [
    'defender',
    'midfielder',
    'forward',
  ],
  flexi_six: [
    'defender',
    'midfielder',
    'forward',
  ],
  hockey: [
    'goalkeeper',
    'defender',
    'forward',
  ],
  basket: [
    'guard_point',
    'guard_shooting',
    'forward_small',
    'forward_power',
    'center',
  ],
  horses: [
    'elite',
    'v75',
    'age3',
    'age4',
    'coldblood',
    'rider',
    'coach',
  ],
  cycling: [
    'manager',
    'captain',
    'tempo',
    'youth',
    'climber',
    'sprinter',
  ],
  cc_ski: [
    'men',
    'women',
    'manager',
  ],
  dota2: [
    'core',
    'support'
  ],
  default: [
    null,
  ],
}

exports.positionArray = Object.keys(exports.positions).sort(function(a, b) {
  return exports.positions[a] - exports.positions[b]
})

// factor
// default: 1
//
// All scores are multiplied with this in order to avoid decimals

// captain
// default: 1
//
// The bonus fraction added to captain score, default is 1
exports.ruleSets = [
  {sport: 'football'},
  {sport: 'poker'},
  {sport: 'hockey', factor: 10},
  {sport: 'basket', factor: 4},
  {sport: 'dota2'},
  {sport: 'golf'},
  {sport: 'horses'},
  {sport: 'cycling'},
  {sport: 'cycling_vm'},
  {sport: 'cc_ski', factor: 2, captain: 0.5},
]

exports.sports = {
  football: 0,
  poker: 10,
  hockey: 20,
  basket: 30,
  dota2: 40,
  golf: 50,
  horses: 60,
  cycling: 70,
  cycling_vm: 80,
  cc_ski: 90,
}
exports.inverseSports = Object.keys(exports.sports).reduce(function(obj, sport) {
  obj[exports.sports[sport]] = sport
  return obj
}, {})

exports.sportsArray = exports.ruleSets.map(function(set) {
  return set.sport
})

var gameTypes = {
  five_a_side: 1,
  six_a_side: 2,
  flexi_five: 3,
  flexi_six: 4,

  hockey_eight_a_side: 21,

  basket_five_a_side: 31,
}
Object.keys(gameTypes).forEach(function(type) {
  if(exports.sportPositions[type]) return
  var sport = exports.inverseSports[Math.floor(gameTypes[type] / 10) * 10]
  exports.sportPositions[type] = exports.sportPositions[sport]
})
exports.gameTypes = Object.assign(gameTypes, exports.sports)

exports.formations = {
  football: {
    defaults: {
      max_players_per_team: 3,
      substitutes: 1,
      goalkeeper_substitutes: 0,
      captains: 2
    },
    max_substitutions: {
      substitutes: 3,
      goalkeeper_substitutes: 1
    },
    min: {
      goalkeeper: 1,
      defender: 3,
      midfielder: 2,
      forward: 1
    },
    max: {
      goalkeeper: 1,
      defender: 5,
      midfielder: 5,
      forward: 3
    },
    max_total: {
      goalkeeper: 2,
      defender: 5,
      midfielder: 5,
      forward: 3
    },
    active: 11
  },
  five_a_side: {
    defaults: {
      max_players_per_team: 4,
      substitutes: 1,
      goalkeeper_substitutes: 0,
      captains: 1
    },
    max_substitutions: {
      substitutes: 3,
      goalkeeper_substitutes: 1
    },
    min: {
      goalkeeper: 1,
      defender: 1,
      midfielder: 1,
      forward: 1
    },
    max: {
      goalkeeper: 1,
      defender: 2,
      midfielder: 2,
      forward: 2
    },
    max_total: {
      goalkeeper: 2,
      defender: 2,
      midfielder: 2,
      forward: 2
    },
    active: 5
  },
  six_a_side: {
    defaults: {
      max_players_per_team: 5,
      substitutes: 1,
      goalkeeper_substitutes: 0,
      captains: 1
    },
    max_substitutions: {
      substitutes: 3,
      goalkeeper_substitutes: 1
    },
    min: {
      goalkeeper: 1,
      defender: 1,
      midfielder: 1,
      forward: 1
    },
    max: {
      goalkeeper: 1,
      defender: 2,
      midfielder: 2,
      forward: 2
    },
    max_total: {
      goalkeeper: 2,
      defender: 2,
      midfielder: 2,
      forward: 2
    },
    active: 6
  },
  flexi_five: {
    defaults: {
      max_players_per_team: 4,
      substitutes: 0,
      goalkeeper_substitutes: 0,
      captains: 1
    },
    max_substitutions: {
      substitutes: 3,
      goalkeeper_substitutes: 0
    },
    min: {
      goalkeeper: 0,
      defender: 0,
      midfielder: 0,
      forward: 0
    },
    max: {
      goalkeeper: 0,
      defender: 5,
      midfielder: 5,
      forward: 3
    },
    max_total: {
      goalkeeper: 0,
      defender: 5,
      midfielder: 5,
      forward: 3
    },
    active: 5
  },
  flexi_six: {
    defaults: {
      max_players_per_team: 5,
      substitutes: 0,
      goalkeeper_substitutes: 0,
      captains: 1
    },
    max_substitutions: {
      substitutes: 3,
      goalkeeper_substitutes: 0
    },
    min: {
      goalkeeper: 0,
      defender: 0,
      midfielder: 0,
      forward: 0
    },
    max: {
      goalkeeper: 0,
      defender: 5,
      midfielder: 5,
      forward: 3
    },
    max_total: {
      goalkeeper: 0,
      defender: 5,
      midfielder: 5,
      forward: 3
    },
    active: 6
  },
  poker: {
    defaults: {
      max_players_per_team: 7,
      substitutes: 1,
      captains: 1
    },
    max_substitutions: {
      substitutes: 1
    },
    min: {
      null: 6,
      null: 6,
    },
    max: {
      null: 7,
      substitutes: 1
    },
    max_total: {
      null: 7
    },
    active: 6
  },
  hockey: {
    defaults: {
      max_players_per_team: 3,
      substitutes: 1,
      goalkeeper_substitutes: 0,
      captains: 0
    },
    max_substitutions: {
      substitutes: 2,
      goalkeeper_substitutes: 1
    },
    min: {
      goalkeeper: 1,
      defender: 2,
      forward: 3
    },
    max: {
      goalkeeper: 1,
      defender: 2,
      forward: 3
    },
    max_total: {
      goalkeeper: 2,
      defender: 3,
      forward: 4
    },
    active: 6
  },
  hockey_eight_a_side: {
    defaults: {
      max_players_per_team: 3,
      substitutes: 1,
      goalkeeper_substitutes: 0,
      captains: 0
    },
    max_substitutions: {
      substitutes: 2,
      goalkeeper_substitutes: 1
    },
    min: {
      goalkeeper: 1,
      defender: 3,
      forward: 4,
    },
    max: {
      goalkeeper: 1,
      defender: 3,
      forward: 4,
    },
    max_total: {
      goalkeeper: 2,
      defender: 4,
      forward: 5,
    },
    active: 8,
  },
  basket: {
    defaults: {
      max_players_per_team: 2,
      substitutes: 2,
      captains: 2,
    },
    max_substitutions: {
      substitutes: 2,
      goalkeeper_substitutes: 0
    },
    min: {
      guard_point: 2,
      guard_shooting: 2,
      forward_small: 2,
      forward_power: 2,
      center: 1
    },
    max: {
      guard_point: 2,
      guard_shooting: 2,
      forward_small: 2,
      forward_power: 2,
      center: 1
    },
    max_total: {
      guard_point: 4,
      guard_shooting: 4,
      forward_small: 4,
      forward_power: 4,
      center: 4
    },
    active: 9
  },
  basket_five_a_side: {
    defaults: {
      captains: 1,
      max_players_per_team: 2,
      substitutes: 1,
      goalkeeper_substitutes: 0
    },
    max_substitutions: {
      substitutes: 1
    },
    min: {
      guard_point: 1,
      guard_shooting: 1,
      forward_small: 1,
      forward_power: 1,
      center: 1
    },
    max: {
      guard_point: 1,
      guard_shooting: 1,
      forward_small: 1,
      forward_power: 1,
      center: 1
    },
    max_total: {
      guard_point: 2,
      guard_shooting: 2,
      forward_small: 2,
      forward_power: 2,
      center: 2
    },
    active: 5
  },
  dota2: {
    defaults: {
      max_players_per_team: 5,
      substitutes: 0,
      goalkeeper_substitutes: 0,
      captains: 0,
    },
    max_substitutions: {
      substitutes: 0
    },
    min: {
      core: 3,
      support: 2
    },
    max: {
      core: 3,
      support: 2
    },
    max_total: {
      core: 3,
      support: 2
    },
    active: 5
  },
  golf: {
    defaults: {
      max_players_per_team: 2,
      substitutes: 0,
      goalkeeper_substitutes: 0,
      captains: 0,
    },
    max_substitutions: {
      substitutes: 1
    },
    min: {
      null: 6
    },
    max: {
      null: 6
    },
    max_total: {
      null: 6
    },
    active: 6
  },
  horses: {
    defaults: {
      max_players_per_team: 7,
      substitutes: 0,
      goalkeeper_substitutes: 0,
      captains: 0,
    },
    max_substitutions: {
    },
    min: {
      elite: 1,
      v75: 1,
      age3: 1,
      age4: 1,
      coldblood: 1,
      rider: 1,
      coach: 1,
    },
    max: {
      elite: 1,
      v75: 1,
      age3: 1,
      age4: 1,
      coldblood: 1,
      rider: 1,
      coach: 1,
    },
    max_total: {
      elite: 1,
      v75: 1,
      age3: 1,
      age4: 1,
      coldblood: 1,
      rider: 1,
      coach: 1,
    },
    active: 7,
  },
  cycling: {
    defaults: {
      max_players_per_team: 3,
      substitutes: 0,
      goalkeeper_substitutes: 0,
      captains: 0,
    },
    max_substitutions: {
    },
    min: {
      manager: 1,
      tempo: 1,
      support: 3,
      captain: 2,
      youth: 2,
      climber: 2,
      sprinter: 2,
    },
    max: {
      manager: 1,
      tempo: 1,
      support: 3,
      captain: 2,
      youth: 2,
      climber: 2,
      sprinter: 2,
    },
    max_total: {
      manager: 1,
      tempo: 1,
      support: 3,
      captain: 2,
      youth: 2,
      climber: 2,
      sprinter: 2,
    },
    active: 13,
  },
  cycling_vm: {
    defaults: {
      max_players_per_team: 5,
      substitutes: 0,
      goalkeeper_substitutes: 0,
      captains: 0,
    },
    max_substitutions: {
    },
    min: {
      null: 5,
    },
    max: {
      null: 5,
    },
    max_total: {
      null: 5,
    },
    active: 5,
  },
  cc_ski: {
    defaults: {
      max_players_per_team: 3,
    },
    max_substitutions: {
    },
    min: {
      men: 4,
      women: 4,
      manager: 1,
    },
    max: {
      men: 4,
      women: 4,
      manager:1,
    },
    max_total: {
      men: 4,
      women: 4,
      manager: 1,
    },
    active: 9,
  },
}

exports.nonDuelSports = [
  'golf',
  'horses',
  'cycling',
  'cycling_vm',
  'cc_ski',
]

exports.racingSports = [
  'horses',
  'cycling',
  'cycling_vm',
  'cc_ski',
]

exports.crossCountrySports = [
  'cycling',
  'cc_ski',
]
